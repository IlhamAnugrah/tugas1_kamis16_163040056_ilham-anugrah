<?php 

/**
 * 
 */
class Barang_model
{

	private $table = 'barang';
	private $db;
	private $nama_barang;
	private $merk;
	private $kategori;
	private $harga;
	private $garansi;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllBarang()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getBarangById($id)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}

	public function deleteBarang($id) {
		$this->db->query("DELETE FROM " . $this->table . " WHERE id=:id");
		$this->db->bind('id', $id);
		$this->db->execute();
	}

	public function addBarang($info) {
		$nama_barang = $info['nama_barang'];
		$merk = $info['merk'];
		$kategori = $info['kategori'];
		$harga = $info['harga'];
		$garansi = $info['garansi'];

		$this->db->query("INSERT INTO "	 . $this->table . "(id, nama_barang, merk, kategori, harga, garansi) VALUES ('', :nama_barang, :merk, :kategori, :harga, :garansi)");
		$this->db->bind('nama_barang', $nama_barang);
		$this->db->bind('merk', $merk);
		$this->db->bind('kategori', $kategori);
		$this->db->bind('harga', $harga);
		$this->db->bind('garansi', $garansi);
		$this->db->execute();
	}

	public function searchBarang($keyword) {
		$this->db->query("SELECT * FROM " . $this->table . " WHERE nama_barang LIKE :keyword OR merk LIKE :keyword OR kategori LIKE :keyword OR harga LIKE :keyword OR garansi LIKE :keyword");
		$this->db->bind('keyword', $keyword);
		return $this->db->resultSet();
	}

	public function changeBarang($info) {
		$id = $info['id'];
		$nama_barang = $info['nama_barang'];
		$merk = $info['merk'];
		$kategori = $info['kategori'];
		$harga = $info['harga'];
		$garansi = $info['garansi'];

		$this->db->query("UPDATE " . $this->table . " SET nama_barang = :nama_barang, merk = :merk, kategori = :kategori, harga = :harga, garansi = :garansi WHERE 
			id = :id");
		$this->db->bind('id', $id);
		$this->db->bind('nama_barang', $nama_barang);
		$this->db->bind('merk', $merk);
		$this->db->bind('kategori', $kategori);
		$this->db->bind('harga', $harga);
		$this->db->bind('garansi', $garansi);
		$this->db->execute();
	}
}