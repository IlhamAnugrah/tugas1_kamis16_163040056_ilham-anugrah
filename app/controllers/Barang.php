<?php 

/**
 * 
 */
class Barang extends Controller {
	public function index() {
		$keyword = "";

		if (isset($_POST['search'])) {
			$keyword = '%' . $_POST['search'] . '%';
			$data = array(
				'judul' => "Daftar Barang",
				'brg' => $this->model('Barang_model')->searchBarang($keyword)
			);
			$this->view('templates/header', $data);
			$this->view('Barang/index', $data);
			$this->view('templates/footer');
		} else {
			$data = array(
				'judul' => "Daftar Barang",
				'brg' => $this->model('Barang_model')->getAllBarang()
			);
		$this->view('templates/header', $data);
		$this->view('barang/index', $data);
		$this->view('templates/footer');
		}
	}

	public function change($id) {

		$data = array(
			'judul' => "Ubah Data Barang",
			'brg' => $this->model('Barang_model')->getBarangById($id)
		);

		if (isset($_POST['submit'])) {
			$info['id'] = $_POST['id'];
			$info['nama_barang'] = $_POST['nama_barang'];
			$info['merk'] = $_POST['merk'];
			$info['kategori'] = $_POST['kategori'];
			$info['harga'] = $_POST['harga'];
			$info['garansi'] = $_POST['garansi'];

			$this->model('Barang_model')->changeBarang($info);
			header("Location: ../index");
		}

		$this->view('templates/header', $data);
		$this->view('barang/change', $data);
		$this->view('templates/footer');
	}

	public function add() {

		$data = array(
			'judul' => "Tambah Barang"
		);

		if (isset($_POST['submit'])) {
			$info['nama_barang'] = $_POST['nama_barang'];
			$info['merk'] = $_POST['merk'];
			$info['kategori'] = $_POST['kategori'];
			$info['harga'] = $_POST['harga'];
			$info['garansi'] = $_POST['garansi'];
			$this->model('Barang_model')->addBarang($info);
			header("Location: Barang/index");
		}

		$this->view('templates/header', $data);
		$this->view('barang/add', $data);
		$this->view('templates/footer');	
	}

	public function delete($id) {
		$this->model('Barang_model')->deleteBarang($id);
		header("Location: ../index");
	}
}