<div class="container mt-4">
	<div>
		<div>
			<h1 class="text-center"><?= $data['judul'] ?></h1>
				<form enctype="multipart/form-data" method="POST" action="<?= BASEURL ?>/barang/change/<?= $data['brg']['id'] ?>">
			  <div class="form-row">
				<input size="30px" type="hidden" id="id" name="id" value="<?= $data['brg']['id'] ?>">
			    <div class="form-group col-md-12">
			      <label for="nama_barang">Nama Barang</label>
			      <input type="text" id="nama_barang" name="nama_barang" class="form-control" value="<?= $data['brg']['nama_barang'] ?>">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="merk">Merk</label>
			      <input type="text" id="merk" name="merk" class="form-control" value="<?= $data['brg']['merk'] ?>">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="kategori">Kategori</label>
			      <input type="text" id="kategori" name="kategori" class="form-control" value="<?= $data['brg']['kategori'] ?>">
			    </div>
			    <div class="form-group col-md-6">
			      <label for="harga">Harga</label>
			      <div class="input-group mb-2">
			      	<div class="input-group-prepend">
			          <div class="input-group-text">Rp.</div>
			        </div>
			      <input type="number" id="harga" name="harga" class="form-control" value="<?= $data['brg']['harga'] ?>">
			      </div>
			    </div>
			    <div class="form-group col-md-6">
			      <label for="garansi">Garansi</label>
			      <input type="text" id="garansi" name="garansi" class="form-control" value="<?= $data['brg']['garansi'] ?>">
			    </div>
			  </div>
				<button type="submit" name="submit" id="submit" class="btn btn-primary">Ubah</button>
				<a href="<?= BASEURL ?>/barang"><button type="button" class="btn">Cancel</button></a>
			</form>
		</div>
	</div>
</div>