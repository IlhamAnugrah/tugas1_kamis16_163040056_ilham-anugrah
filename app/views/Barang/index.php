<div class="container mt-5">
	<div>
		<div>
			<h1 class="text-center"><?= $data['judul'] ?></h1>
			<div>
				<a href="<?= BASEURL ?>/barang/add"><button type="button" class="btn btn-primary">Tambah Data</button></a>	
			</div>
			<table class="table table-striped mt-3">
			  <thead class="text-center">
			    <tr class="text-center">
			      <th scope="col">No.</th>
			      <th scope="col">Nama Barang</th>
			      <th scope="col">Merk</th>
			      <th scope="col">Kategori</th>
			      <th scope="col">Harga</th>
			      <th scope="col">Garansi</th>
			      <th scope="col">Aksi</th>
			    </tr>
			  </thead>
			  <tbody class="text-center">
			  	<?php if (empty($data['brg'])) { ?>
				<tr>
					<td align="center" colspan="7">Maaf, Data tidak ditemukan</td>
				</tr>
				<?php } else { ?>
				<?php $i = 1; foreach ($data['brg'] as $key) : ?>
			    <tr>
			      <th scope="row"><?= $i; ?></th>
			     	<td><?= $key['nama_barang']?></td>
					<td><?= $key['merk']?></td>
					<td><?= $key['kategori']?></td>
					<td><?= $key['harga']?></td>
					<td><?= $key['garansi']?></td>
					<td>
						<a href="<?= BASEURL ?>/barang/change/<?= $key['id'] ?>"><button type="button" class="btn btn-sm btn-success">Change</button></a>
					
						<a href="<?= BASEURL ?>/barang/delete/<?= $key['id'] ?>"><button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini?')">Delete</button></a>
					</td>
			    </tr>
			    <?php $i++; endforeach; ?>
				<?php } ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

