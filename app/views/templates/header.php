<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Halaman <?= $data['judul'] ?></title>
	<link rel="stylesheet" type="text/css" href="<?= BASEURL ?>/css/bootstrap.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>

	<nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="<?= BASEURL; ?>">Electronical - Admin</a>
	  <form class="form-inline" role="search" enctype="multipart/form-data" method="POST">
	    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" id="search" name="search">
	    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
	  </form>
	</nav>